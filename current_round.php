<?php

require("./application/service/bingo.php");

$bingo = new Bingo();
$activeRound = $bingo->round->id;

if(isset($_POST['current_round'])) {
	$userRound = $_POST['current_round'];

	if ($activeRound != $userRound) {
		echo "refresh";
	} else {
		echo "same";
	}
}

?>