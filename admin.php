<?php 

require("./application/service/bingo.php");
require("elements/header.php");

$bingo = new Bingo();
$user_ip = $_SERVER['REMOTE_ADDR'];

// 86.91.152.251 = Matthias
if($user_ip == '83.87.58.90' || $user_ip == "192.168.178.1" || $user_ip == '::1' || $user_ip == "86.91.152.251") {

$round_id = null;
$session_id = null;

function build_query($round_id = null, $session_id = null) {
	$query['round_id'] = $round_id;
	$query['session_id'] = $session_id;
	echo http_build_query($query);
}

if(isset($_GET['create'])) {
	$bingo->create_round();
	header("location: admin.php");
}

?>

<script>
function create_new_round() {
	var r = confirm("Are you sure?");
	if (r == true) {
	  location.href='admin.php?create=true';
	}
}
</script>

<div class="container admin">
	<div class="row title">
		<div class="col-sm-12 text-center">
			<h1>Admin panel</h1>
		</div>
	</div>
	<div class="row buttons">
		<div class="col-md-6 text-center">
			<button onclick="create_new_round()" class="btn btn-primary">Start new round</button>
		</div>
		<div class="col-md-6 text-center">
			<button onclick="location.href='admin.php'" class="btn btn-secondary">Reset view</button>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 col-lg-3">
			<h2>Rounds</h2>
			<table class="table">
			    <thead>
				    <tr>
					    <th scope="col">ID</th>
					    <th scope="col">Active</th>
					    <th scope="col">Created</th>
				    </tr>
			    </thead>
				<tbody>
					<?php 
					$rounds = $bingo->load_all_rounds();
					if(!empty($rounds)) {

						foreach($rounds as $round) { ?>

							<tr onClick="window.location = './admin.php?<?php 
							if(isset($_GET['session_id'])) {
								$session_id = $_GET['session_id'];
							}
							build_query($round->id, $session_id);?>'" class="rounds <?php if($_GET['round_id'] == $round->id) { echo "active"; } ?>">
								<th scope="row"><?php echo $round->id;?></th>
								<td><?php echo $round->active;?></td>
								<td><?php echo $round->created;?></td>
							</tr>
						<?php 
							}
						}
					?>
				</tbody>
			</table>
		</div>
		<div class="col-md-6 col-lg-5">
			<h2>Sessions</h2>
			<table class="table">
			    <thead>
				    <tr>
					    <th scope="col">ID</th>
					    <th scope="col">Name</th>
					    <th scope="col">IP</th>
					    <th scope="col">Created</th>
				    </tr>
			    </thead>
				<tbody>
					<?php 
					if (isset($_GET['round_id'])) {

						$sessions = $bingo->load_sessions_in_round($_GET['round_id']);
					} else {

						$sessions = $bingo->load_all_sessions();
					}
					if(!empty($sessions)) {

						foreach($sessions as $session) { ?>

						<tr onClick="window.location = './admin.php?<?php 
						if(isset($_GET['round_id'])) {
								$round_id = $_GET['round_id'];
							}
						build_query($round_id, $session->id);?>'" class="user-rows <?php if($_GET['session_id'] == $session->id) { echo "active"; } ?>">
							<th scope="row"><?php echo $session->id;?></th>
							<td><?php echo $session->name;?></td>
							<td><?php echo $session->ip;?></td>
							<td><?php echo $session->created;?></td>
						</tr>
					<?php 	
						}
					}
					?>
				</tbody>
			</table>
		</div>
		<div class="col-md-6 col-lg-4">
			<h2 class="text-center">Card</h2>
			<div class="bingocard">
				<div class="container wrapper-bingo">
					<div class="row pinch">
					<?php 
					if (isset($_GET['session_id']) && isset($_GET['round_id'])) { 

						$card = $bingo->load_card_from_round_and_session($_GET['session_id'], $_GET['round_id']);
						if ($card) {
							foreach($card->numbers as $number) { ?>
							
							<div id="bingo-<?php echo strtolower($number->value); ?>" number="<?php echo $number->value; ?>" class="vakje <?php if($number->checked) {echo "bg";} ?>">
									<label for="name"><?php echo $number->value; ?></label>
							</div>
					
						<?php 
							}
						}
					} 
					?>
					</div>
					<div class="container text-center">
						<?php
							if (isset($_GET['session_id']) && isset($_GET['round_id'])) { 

								$card = $bingo->load_card_from_round_and_session($_GET['session_id'], $_GET['round_id']);
								echo '<p>' . $card->bingo . '</p>';
							}
						?>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>

<?php } //close access check to page ?>
<?php require "elements/footer.php";
?>