<?php

require("./application/config.php");
require("./application/model/session.php");
require("./application/model/round.php");
require("./application/model/card.php");

class Bingo {

	// Database connection
	var $connection;

	// Current session
	var $session;

	// Current round
	var $round;

	// Current card
	var $card;

	function __construct() {

		global $mysql_host, $mysql_username, $mysql_password, $mysql_database;

		// Initialize database connection
		$this->connection = new mysqli($mysql_host, $mysql_username, $mysql_password, $mysql_database);

		if(!$this->connection) {
			die("Connection to MySQL database failed: ". $this->connection->connect_error());
		}

		$this->load_session();
		$this->load_round();
	}

	/**
	Loads the session for a user based on the cookie
	*/
	function load_session() {

		global $cookie_session;

		$id = @$_COOKIE[$cookie_session];

		// Make sure we have an ID
		if(!$id) {
			return;
		}

		$session = Session::fetch($this->connection, $id);

		// Make sure the session is being accessed from the same IP
		if(isset($session) && $session->ip == $_SERVER['REMOTE_ADDR']) {

			$this->session = $session;
		}
	}

	/**
	Creates a session for a user, also save a cookie
	*/
	function create_session($name) {

		// Don't login if a cookie exists
		if($this->session) {
			return;
		}

		global $cookie_session;

		// Persist the session
		$this->session = Session::create($this->connection, $name, $_SERVER['REMOTE_ADDR']);
		setcookie($cookie_session, $this->session->id, time() + (60 * 60 * 48));

		// Reload the card
		$this->load_card();
	}

	/**
	Load the current active round
	*/
	function load_round() {

		$this->round = Round::fetch_active($this->connection);

		// Try to make sure there is always an active round
		if(!$this->round) {

			$this->round = Round::create($this->connection, 1);
		}

		// Load a card for the current round
		$this->load_card();
	}

	/**
	Load all rounds
	*/
	function load_all_rounds() {

		return Round::get_all_rounds($this->connection);
	}

	/**
	Load all sessions
	*/
	function load_all_sessions() {

		return Session::fetch_all($this->connection);
	}

	/**
	Load card based on session and round
	*/
	function load_card_from_round_and_session($session_id, $round_id) {

		$session = Session::fetch($this->connection, $session_id);
		$round = Round::fetch($this->connection, $round_id);
		return Card::fetch_for_session_in_round($this->connection, $session, $round);
	}

	/**
	Load all sessions based on a round id
	*/
	function load_sessions_in_round($round_id) {

		$round = Round::fetch($this->connection, $round_id);
		return Session::fetch_all_in_round($this->connection, $round);
	}

	/**
	Loads the current card based on the session and round
	*/
	function load_card() {

		// Make sure we have a session and round
		if(!$this->session || !$this->round) {
			return;
		}

		$this->card = Card::fetch_for_session_in_round($this->connection, $this->session, $this->round);

		if(!$this->card) {

			$this->create_card();
		}
	}

	/**
	Creates a new card based on the round and session
	*/
	function create_card() {

		// Make sure we have a session and round
		if(!$this->session || !$this->round) {
			return;
		}

		$numbers = range(1,75);
		shuffle($numbers);
		$random_keys = array_rand($numbers, 24);
		shuffle($random_keys);

		$random_numbers = array_map(function($n) use ($numbers) {
			return $numbers[$n];
		}, $random_keys);

		array_splice($random_numbers, 12, 0, "Joker");

		$this->card = Card::create($this->connection, $this->session, $this->round, $random_numbers);
	}

	/**
	Saves the card instance
	*/
	function save_card() {

		// Make sure we have a card to save
		if(!isset($this->card)) {
			return;
		}

		$this->card->save($this->connection);
	}

	function create_round() {
		
		Round::create($this->connection);
	}
}

