<?php

class CardNumber {
	
	var $value;
	var $checked;
}

class Card {

	var $id;
	var $session_id;
	var $round_id;
	var $numbers;
	var $bingo;
	var $created;

	function check($number) {

		$index = $this->number_index($number);

		if($index === false) {
			return;
		}

		$this->numbers[$index]->checked = true;
	}

	function uncheck($number) {

		$index = $this->number_index($number);

		if($index === false) {
			return;
		}

		$this->numbers[$index]->checked = false;
	}

	function toggle($number) {

		$index = $this->number_index($number);

		if($index === false) {
			return;
		}

		$this->numbers[$index]->checked = !$this->numbers[$index]->checked;
	}

	private function number_values() {

		return array_map(function($n) {

			return $n->value;
		}, $this->numbers);
	}

	private function number_index($number) {

		return array_search($number, $this->number_values());
	}

	/**
	Saves the current card back into the database
	*/
	function save($connection) {

		$checked_numbers_filtered = array_filter($this->numbers, function($n) {

			return $n->checked;
		});


		$checked_numbers_mapped = array_map(function($n) {

			return $n->value;
		}, $checked_numbers_filtered);

		$checked_numbers = implode($checked_numbers_mapped, ",");

		// Calculate extra query for bingo
		$is_bingo = count(array_filter($this->numbers, function($c) {

			return $c->checked == false;
		})) == 0;

		$is_bingo_query = "";

		if($is_bingo == true) {

			$is_bingo_query = ", bingo = now(3)";
		}

		$query = "UPDATE `card` SET checked_numbers = '". $checked_numbers ."'". $is_bingo_query ." WHERE id = '". $this->id ."'";
		$result = $connection->query($query);

		if(!$result) {
			die($connection->error);
		}
	}

	/**
	Creates a new card in the database, based on a session, round and numbers
	*/
	static function create($connection, $session, $round, $numbers) {

		$query = "INSERT INTO `card` (session_id, round_id, generated_numbers) VALUES ('". $session->id ."', '". $round->id ."' ,'". implode($numbers, ",") ."')";
		$result = $connection->query($query);

		if(!$result) {
			die($connection->error);
		}

		return Card::fetch($connection, $connection->insert_id);
	}

	/**
	Fetches a card based on a round
	*/
	static function fetch_for_session_in_round($connection, $session, $round) {

		$query = "SELECT id, session_id, round_id, generated_numbers, checked_numbers, bingo, created FROM `card` WHERE round_id = '". $round->id ."' AND session_id = '". $session->id ."'";
		$result = $connection->query($query);

		if(!$result) {
			die($connection->error);
		}

		if($result->num_rows == 0) {
			return null;
		}

		$card = $result->fetch_object("Card");
		return Card::parse($card);
	}

	/**
	Fetches a card based on an ID
	*/
	static function fetch($connection, $id) {

		$query = "SELECT id, session_id, round_id, generated_numbers, checked_numbers, bingo, created FROM `card` WHERE id = '". $id ."'";
		$result = $connection->query($query);

		if(!$result) {
			die($connection->error);
		}

		if($result->num_rows == 0) {
			return null;
		}

		$card = $result->fetch_object("Card");
		return Card::parse($card);
	}

	/**
	Parses a database card object
	*/
	private static function parse($card) {

		$generated_numbers = explode(",", $card->generated_numbers);
		$checked_numbers = explode(",", $card->checked_numbers);

		foreach($generated_numbers as $generated_number) {

			$number = new CardNumber();
			$number->value = $generated_number;
			$number->checked = in_array($generated_number, $checked_numbers);
			$card->numbers[] = $number;
		}

		// Manual cleanup
		unset($card->checked_numbers);
		unset($card->generated_numbers);

		return $card;
	}
}
