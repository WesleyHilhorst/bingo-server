<?php

class Session {

	var $id;
	var $name;
	var $ip;
	var $created;

	/**
	Creates a session based on a name and ip
	*/
	static function create($connection, $name, $ip) {

		$query = "INSERT INTO `session` (name, ip) VALUES ('". $connection->real_escape_string($name) ."', '". $ip ."')";
		$result = $connection->query($query);

		if(!$result) {
			die($connection->error);
		}

		return Session::fetch($connection, $connection->insert_id);
	}

	/**
	Fetches all cards that are active
	*/
	static function fetch_all_in_round($connection, $round) {

		$query = "SELECT s.id, s.name, s.ip, s.created FROM `session` as `s` INNER JOIN `card` as `c` ON (s.id = c.session_id) WHERE c.round_id = '". $round->id ."' ORDER BY ip, name ASC";
		$result = $connection->query($query);

		if(!$result) {
			die($connection->error);
		}

		if($result->num_rows == 0) {
			return null;
		}

		$sessions = array();
		
		while($session = $result->fetch_object("Session")) {
			$sessions[] = $session;
		}

		return $sessions;
	}

	/**
	Fetches all sessions that are active
	*/
	static function fetch_all($connection) {

		$query = "SELECT id, name, ip, created FROM `session` ORDER BY ip, name ASC";
		$result = $connection->query($query);

		if(!$result) {
			die($connection->error);
		}

		if($result->num_rows == 0) {
			return null;
		}

		$sessions = array();
		
		while($session = $result->fetch_object("Session")) {
			$sessions[] = $session;
		}

		return $sessions;
	}

	/**
	Fetches a session based on an ID
	*/
	static function fetch($connection, $id) {

		$query = "SELECT id, name, ip, created FROM `session` WHERE id = '". $id ."'";
		$result = $connection->query($query);

		if(!$result) {
			die($connection->error);
		}

		if($result->num_rows == 0) {
			return null;
		}

		return $result->fetch_object("Session");
	}
}

