<?php

class Round {

	var $id;
	var $active;
	var $created;

	/**
	Creates a round in the database
	@param active - whether this is the active round
	*/
	static function create($connection) {

		// Disable other active rounds

		$query = "UPDATE `round` SET active = '0'";
		$connection->query($query);

		$query = "INSERT INTO `round` (active) VALUES (1)";
		$result = $connection->query($query);

		if(!$result) {
			die($connection->error);
		}

		return Round::fetch($connection, $connection->insert_id);
	}

	/**
	Fetches the currently active round from the database
	*/
	static function fetch_active($connection) {

		$query = "SELECT id, active, created FROM `round` WHERE active = '1'";
		$result = $connection->query($query);

		if(!$result) {
			die($connection->error);
		}

		if($result->num_rows == 0) {
			return null;
		}

		return $result->fetch_object("Round");
	}

	/**
	Fetch a round from the datbase based on an ID
	*/
	static function fetch($connection, $id) {

		$query = "SELECT id, active, created FROM `round` WHERE id = '". $id ."'";
		$result = $connection->query($query);

		if(!$result) {
			die($connection->error);
		}

		if($result->num_rows == 0) {
			return null;
		}

		return $result->fetch_object("Round");
	}

	static function get_all_rounds($connection) {

		$query = "SELECT id, active, created FROM `round` ORDER BY id DESC";
		$result = $connection->query($query);

		if(!$result) {
			die($connection->error);
		}

		if($result->num_rows == 0) {
			return null;
		}

		$rounds = array();
		
		while($round = $result->fetch_object("Round")) {
			$rounds[] = $round;
		}

		return $rounds;
	}
}

