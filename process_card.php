<?php

require("./application/service/bingo.php");

$bingo = new Bingo();

// Check round
if(isset($_POST['current_round']) && $bingo->round->id != $_POST['current_round']) {
	
	echo "refresh";
} else if(isset($_POST['number']) && isset($_POST['checked'])) {

	if($_POST['checked'] == "true" ) {

		$bingo->card->check($_POST['number']);
	} else {

		$bingo->card->uncheck($_POST['number']);
	}

	$bingo->save_card();
}

?>