<?php 

require("./application/service/bingo.php");

$bingo = new Bingo();

// Handle login
if(isset($_POST['name'])) {
	$bingo->create_session(htmlentities($_POST['name']));
	header("location: ./");
}

require("elements/header.php");
?>
<script>
set_current_round(<?php echo $bingo->round->id; ?>);
</script>

<div class="container margin-top">
	<div class="row justify-content-md-center">
		<div class="col-md-7 col-lg-5">
			
		<?php if(!$bingo->session) { ?>
			
				<h1 class="text-center">Bingo!</h1>
				<form action="index.php" method="post" name="login" class="login-form">
					<div class="form-group">
						<input class="form-control" type="text" name="name" placeholder="Enter your name" required>
					</div>
					<div class="form-group">
						<input class="btn btn-primary" type="submit" name="submit" value="Start!">
					</div>
				</form>

		<?php } else { ?>
			
			<h1 class="text-center">Hi <?php echo ucfirst($bingo->session->name); ?>!</h1>
			<div class="container text-center">
				<p>We're now playing round <?php echo $bingo->round->id; ?></p>
			</div>
			<div class="player bingocard">
				<div class="container wrapper-bingo">
					<div class="row bingo">
						<div class="bingo-row">B</div>
						<div class="bingo-row">I</div>
						<div class="bingo-row">N</div>
						<div class="bingo-row">G</div>
						<div class="bingo-row">O</div>
					</div>
					<div class="row pinch">
						<?php 
						$numbers = $bingo->card->numbers;

						foreach ($numbers as $number) { ?>

						
						<div id="bingo-<?php echo strtolower($number->value); ?>" number="<?php echo $number->value; ?>" class="vakje <?php if($number->checked) {echo "bg";} ?>">
								<label for="name"><?php echo $number->value; ?></label>
						</div>
						<?php } ?>
					</div>							
				</div>
				<div class="container">
					<div class="message text-center"></div>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</div>

<?php require "elements/footer.php";
?>