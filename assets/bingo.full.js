// Current round
var current_round = null;

// Loading state
var toggleNumberInProgress = false;
var checkRoundInprogress = false;

// Intervals
var checkRoundInterval = null;
var countdownRoundInterval = null;

function set_current_round(round) {

	current_round = round;
	check_current_round(round);
}

//ID for confetti animation
let requestID;

function toggle_number(element) {

	if(toggleNumberInProgress) {
		return;
	}

	toggleNumberInProgress = true;

	element.toggleClass("bg");

	var number = element.attr('number');
	var checked = element.hasClass("bg");

	$.ajax({
		url: "./process_card.php",
		method: "POST",
		data: { "checked" : checked, "number": number, "current_round": current_round },
		success: function(result) {

    		if(result == "refresh") {

    			// Make the user see the proper state
    			$(".player.bingocard .vakje").unbind('click');
    			element.toggleClass("bg");
				countdown();
			} else {

				toggleNumberInProgress = false;				
			}
  		}
	})

	var numItems = $('.player.bingocard .vakje.bg').length;

	if(numItems == 25) {

		$('.player.bingocard .vakje').addClass("bg-bingo");
		confetti_bingo();
	} else {
		
        $('.player.bingocard .vakje').removeClass("bg-bingo");
        cancelAnimationFrame(requestID);
    }
}



function confetti_bingo() {

		var end = Date.now() + (10 * 1000);

		// go Buckeyes!
		var colors = ['#38ff8e', '#ffffff'];

		(function frame() {
		  confetti({
		    particleCount: 4,
		    angle: 60,
		    spread: 55,
		    origin: { x: 0 },
		    colors: colors
		  });
		  confetti({
		    particleCount: 4,
		    angle: 120,
		    spread: 55,
		    origin: { x: 1 },
		    colors: colors
		  });

		  if (Date.now() < end) {
		    requestID = requestAnimationFrame(frame);
		  }

		}()); 
}

function check_current_round() {

	checkRoundInterval = window.setInterval(function(){

		if(checkRoundInprogress) {
			return;
		}

		checkRoundInprogress = true

		$.ajax({
			url: "./current_round.php",
			method: "POST",
			data: { "current_round" : current_round },
			success: function(result) {

				checkRoundInprogress = false;

				if(result == "refresh") {
					window.clearInterval(checkRoundInterval);
					countdown();
				}
			}
		})
	}, 10000);
}

function countdown() {

	if(countdownRoundInterval) {
		return;
	}

	$(".message").html('<p>New round started, reloading page in <span class="counter">5</span> seconds</p>');

	var counter = 5;
	countdownRoundInterval = window.setInterval(function(){

		counter--;
		$(".counter").html(counter);

		if(counter == 0) {
			location.reload(true);
		}
	}, 1000);
};

function calculate_height() {

	var containerWidth = $('.pinch').outerWidth();
	$('.pinch').outerHeight(containerWidth);
}

function print_warning() {

	console.log(`%c 				 uuuuuuu
             uu$$$$$$$$$$$uu
          uu$$$$$$$$$$$$$$$$$uu
         u$$$$$$$$$$$$$$$$$$$$$u
        u$$$$$$$$$$$$$$$$$$$$$$$u
       u$$$$$$$$$$$$$$$$$$$$$$$$$u
       u$$$$$$$$$$$$$$$$$$$$$$$$$u
       u$$$$$$"   "$$$"   "$$$$$$u
       "$$$$"      u$u       $$$$"
        $$$u       u$u       u$$$
        $$$u      u$$$u      u$$$
         "$$$$uu$$$   $$$uu$$$$"
          "$$$$$$$"   "$$$$$$$"
            u$$$$$$$u$$$$$$$u
             u$"$"$"$"$"$"$u
  uuu        $$u$ $ $ $ $u$$       uuu
 u$$$$        $$$$$u$u$u$$$       u$$$$
  $$$$$uu      "$$$$$$$$$"     uu$$$$$$
u$$$$$$$$$$$uu    """""    uuuu$$$$$$$$$$
$$$$"""$$$$$$$$$$uuu   uu$$$$$$$$$"""$$$"
 """      ""$$$$$$$$$$$uu ""$"""
           uuuu ""$$$$$$$$$$uuu
  u$$$uuu$$$$$$$$$uu ""$$$$$$$$$$$uuu$$$
  $$$$$$$$$$""""           ""$$$$$$$$$$$"
   "$$$$$"                      ""$$$$""
     $$$"                         $$$$""`, "font-family:monospace");

	console.log("Why are you looking here? You shouldn't!!!");
	console.log("Your attempt to do things has been logged.");
}

$(document).ready(function() {

	calculate_height();
	print_warning();
});

$(window).on('load', function () {

	calculate_height();
});

$(document).on("click", ".player.bingocard .vakje", function (event) {

	toggle_number($(this));
});
